package br.com.cursoalura.forum.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cursoalura.forum.model.Topico;

public interface TopicoRepository extends JpaRepository<Topico, Long>{

	// Busca pelo atributo 'nome' da entidade Curso. Outra maneira: findByCurso_Nome
	Page<Topico> findByCursoNome(String nomeCurso, Pageable paginacao);

}
