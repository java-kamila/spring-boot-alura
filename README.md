# Spring Boot - Curso Alura
> <i>Construa uma API Rest</i>

### Anotações

 - Não é uma boa prática retornar entidades JPA nos métodos dos controllers, sendo mais indicado retornar classes que seguem o padrão DTO (Data Transfer Object).
 - Para um método no controller não encaminhar a requisição a uma página JSP, ou Thymeleaf, devemos utilizar a anotação `@ResponseBody`.
 - Sem a anotação @ResponseBody no método do controller, o Spring entende que desejamos fazer uma navegação para uma página. Para evitar isso utilizamos `@RestController` na classe. Assim, a classe assume que todo método utiliza `@ResponseBody`.
 - O Spring, por padrão, converte os dados no formato JSON, utilizando a biblioteca Jackson.
 - `@Enumerated` para salvar o nome da constante do enum e não a ordem de declaração.
 - Para que o Spring Boot popule automaticamente o banco de dados da aplicação, podemos criar o arquivo src/main/resources/data.sql com o script de inserção.
 - Para receber dados enviados no corpo da requisição, a boa prática é criar uma classe que também siga o padrão DTO (Data Transfer Object);
 - A boa prática para métodos que cadastram informações é devolver o código HTTP 201, ao invés do código 200.
 - Para montar uma resposta a ser devolvida ao cliente da API, devemos utilizar a classe `ResponseEntity` do Spring
  - Para receber parâmetros dinâmicos no path da URL, devemos utilizar a anotação `@PathVariable`
 
##### Validação 
 - Para fazer validações das informações enviadas pelos clientes da API, podemos utilizar a especificação `Bean Validation`, com as anotações `@NotNull`, `@NotEmpty`, `@Size`, dentre outras.
 - Para o Spring disparar as validações do Bean Validation e devolver um erro 400, caso alguma informação enviada pelo cliente esteja inválida, devemos utilizar a anotação `@Valid` na requisição
	 
	```
	<dependency> 
		    <groupId>org.springframework.boot</groupId> 
		    <artifactId>spring-boot-starter-validation</artifactId> 
	</dependency>
	```
##### Centralizador de Exceções
	
 - Para interceptar as exceptions que forem lançadas nos métodos das classes controller, devemos criar uma classe anotada com `@RestControllerAdvice`.
 
 
 - Para fazer o controle transacional automático, devemos utilizar a anotação `@Transactional` nos métodos do controller.
 - Ao manupularmos um objeto retornado pelo Repository, podemos afirmar ao Spring para persistir esse dado em banco de dados, dizer pra ele commitar aquela transação em banco através na annotation `@Transactional` (javax).
 
##### Optional 
 - O método `getOne` lança uma exception quando o id passado como parâmetro não existir no banco de dados.
 - O método `findById` retorna um objeto Optional<>, que pode ou não conter um objeto.
 - Para capturar o objeto em um Optional utilizamos o método `optional.get()`;
 
##### Paginação
 - Para realizar paginação com Spring Data JPA, devemos utilizar a interface `Pageable`.
 - Modelo de requisição que recebe objeto Pageable: `http://localhost:8080/topicos?page=0&size=2&sort=id,desc`
 - `@PageableDefault` indica o padrão de paginação caso não sejam definidos pelo cliente.
 - Para fazer a ordenação na consulta ao banco de dados, devemos utilizar também a interface Pageable, passando como parâmetro a direção da ordenação, utilizando a classe Direction, e o nome do atributo para ordenar;
Para receber os parâmetros de ordenação e paginação diretamente nos métodos do controller, devemos habilitar o módulo SpringDataWebSupport, adicionando a anotação `@EnableSpringDataWebSupport` na classe CursoaluraApplication.

##### Cache
 - Para utilizar o módulo de cache do Spring Boot, devemos adicioná-lo como dependência do projeto no arquivo pom.xml;
 - Para habilitar o uso de caches na aplicação, devemos adicionar a anotação `@EnableCaching` na classe ForumApplication;
 - Para que o Spring guarde o retorno de um método no cache, devemos anotá-lo com `@Cacheable`;
 - Para o Spring invalidar algum cache após um determinado método ser chamado, devemos anotá-lo com `@CacheEvict`;
 - Devemos utilizar cache apenas para as informações que nunca ou raramente são atualizadas no banco de dados.
 
##### Spring Security
- O padrão dele é bloquear todos os endpoints
 - Para utilizar o módulo do Spring Security, devemos adicioná-lo como dependência do projeto no arquivo pom.xml;
 - Para habilitar e configurar o controle de autenticação e autorização do projeto, devemos criar uma classe e anotá-la com @Configuration e @EnableWebSecurity;
 - Para liberar acesso a algum endpoint da nossa API, devemos chamar o método http.authorizeRequests().antMatchers().permitAll() dentro do método configure(HttpSecurity http), que está na classe SecurityConfigurations;
 - O método anyRequest().authenticated() indica ao Spring Security para bloquear todos os endpoints que não foram liberados anteriormente com o método permitAll();
 - Para implementar o controle de autenticação na API, devemos implementar a interface UserDetails na classe Usuario e também implementar a interface GrantedAuthority na classe Perfil;
 - Para o Spring Security gerar automaticamente um formulário de login, devemos chamar o método and().formLogin(), dentro do método configure(HttpSecurity http), que está na classe SecurityConfigurations;
 - A lógica de autenticação, que consulta o usuário no banco de dados, deve implementar a interface UserDetailsService;
 - Devemos indicar ao Spring Security qual o algoritmo de hashing de senha que utilizaremos na API, chamando o método passwordEncoder(), dentro do método configure(AuthenticationManagerBuilder auth), que está na classe SecurityConfigurations.

##### Gerando Token com JWT
 - Em uma API Rest, não é uma boa prática utilizar autenticação com o uso de session;
 - Uma das maneiras de fazer autenticação stateless é utilizando tokens JWT (Json Web Token);
 - Para utilizar JWT na API, devemos adicionar a dependência da biblioteca jjwt no arquivo pom.xml do projeto;
 - Para configurar a autenticação stateless no Spring Security, devemos utilizar o método `sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)`;
 - Para disparar manualmente o processo de autenticação no Spring Security, devemos utilizar a classe AuthenticationManager;
 - Para poder injetar o `AuthenticationManager` no controller, devemos criar um método anotado com @Bean, na classe SecurityConfigurations, que retorna uma chamada ao método super.authenticationManager();
 - Para criar o token JWT, devemos utilizar a classe Jwts;
 - O token tem um período de expiração, que pode ser definida no arquivo application.properties;
 - Para injetar uma propriedade do arquivo **application.properties**, devemos utilizar a anotação @Value.

##### Autenticação via JWT
 - Para enviar o token JWT na requisição, é necessário adicionar o cabeçalho Authorization, passando como valor **Bearer** token;
 - Para criar um filtro no Spring, devemos criar uma classe que herda da classe OncePerRequestFilter;
 - Para recuperar o token JWT da requisição no filter, devemos chamar o método `request.getHeader("Authorization")`;
 - Para habilitar o filtro no **Spring Security**, devemos chamar o método `and().addFilterBefore(new AutenticacaoViaTokenFilter(), UsernamePasswordAuthenticationFilter.class)`;
 - Para indicar ao Spring Security que o cliente está autenticado, devemos utilizar a classe SecurityContextHolder, chamando o método `SecurityContextHolder.getContext().setAuthentication(authentication)`.

##### Monitoramento com Spring Boot Actuator
 - Para adicionar o Spring Boot Actuator no projeto, devemos adicioná-lo como uma dependência no arquivo pom.xml;
 - Para acessar as informações disponibilizadas pelo Actuator, devemos entrar no endereço http://localhost:8080/actuator;
 - Para liberar acesso ao **Actuator** no **Spring Security**, devemos chamar o método `.antMatchers(HttpMethod.GET, "/actuator/**")`;
 - Para que o Actuator exponha mais informações sobre a API, devemos adicionar as propriedades `management.endpoint.health.show-details=always` e `management.endpoints.web.exposure.include=*` no arquivo application.properties;
 - Para utilizar o Spring Boot Admin, devemos criar um projeto Spring Boot e adicionar nele os módulos spring-boot-starter-web e spring-boot-admin-server;
 - Para trocar a porta na qual o servidor do Spring Boot Admin rodará, devemos adicionar a propriedade server.port=8081 no arquivo application.properties;
 - Para o **Spring Boot Admin** conseguir monitorar a nossa API, devemos adicionar no projeto da API o módulo spring-boot-admin-client e também adicionar a propriedade spring.boot.admin.client.url=http://localhost:8081 no arquivo **application.properties**;
 - Para acessar a interface gráfica do Spring Boot Admin, devemos entrar no endereço http://localhost:8081.
 - Outro Projeto Spring Boot foi criado para monitoramento deste, segundo [Spring-Boot-Admin](https://codecentric.github.io/spring-boot-admin/2.2.0/). 
 
 
##### Documentação da API com Swagger
 - Para documentar a nossa API Rest, podemos utilizar o Swagger, com o módulo **SpringFox Swagger**;
 - Para utilizar o SpringFox Swagger na API, devemos adicionar suas dependências no arquivo **pom.xml**;
 - Para habilitar o Swagger na API, devemos adicionar a anotação `@EnableSwagger2` na classe ForumApplication;
 - As configurações do Swagger devem ser feitas criando-se uma classe chamada `SwaggerConfigurations` e adicionando nela a anotação `@Configuration`;
 - Para configurar quais endpoints e pacotes da API o Swagger deve gerar a documentação, devemos criar um método anotado com @Bean, que devolve um objeto do tipo `Docket`;
 - Para acessar a documentação da API, devemos entrar no endereço `http://localhost:8080/swagger-ui.html`;
 - Para liberar acesso ao Swagger no Spring Security, devemos chamar o seguinte método `web.ignoring().antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**")`, dentro do método void configure(WebSecurity web), que está na classe `SecurityConfigurations`.

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/maven-plugin/reference/html/#build-image)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/reference/htmlsingle/#using-boot-devtools)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)